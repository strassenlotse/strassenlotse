![LOGO](doc/strassenlotse_logo_wide.png)

# Strassenlotse

Welcome to strassenlotse, a project dedicated to provide usefull information and services to the homeless.

## Contact

The best way to contact us at the moment is via eMail [info@codeforhamburg.org](mailto:info@codeforhamburg.org?subject=Strassenlotse)

## Prerequisites

### Directus instance

Strassenlotse is run by a directus backend. Directus can be set up with different types of databases, we strongly recommed PostgreSQL. For more information on how to set up und run directus check [this guide](https://docs.directus.io/self-hosted/installation.html#installation)

# Frontend

## Setup

After checking out the srtassenlotse repo, you need to install necessary packages:

```
yarn install
```

### Compiles and hot-reloads for development
For your local development, you can set up a server like so:

```
yarn dev
```

### Compiles and minifies for production
For production, build the website like this and deploy to your prefered webhoster:

```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Check language files
```
yarn i18n:report
```

# Backend

## Necessary database changes

strassenlotse needs the postgis extension to run, run this query in your PostgreSQL instance:

```
CREATE EXTENSION postgis;
```

## Strassenlotse configuration

- ask for access to the test.strassenlotse.cloud instance, let them create a test tenant
- configure the `.env.sample` file and save it as `.env` with your tenant id and host information
- add test data to the directus instance
