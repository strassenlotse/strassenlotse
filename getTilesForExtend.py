from __future__ import print_function
import mercantile
import urllib.request
import time
import os

# original code: https://gist.github.com/perrygeo/c52185612b4b1869a35a

if __name__ == "__main__":
    zooms = [13,14,15]
    bounds = 9.6094,53.3563,10.5003,53.73556  # W, S, E, N
    tileurl = 'https://tile.openstreetmap.de'
    output_dir = '/mnt/c/Users/qdood/Projects/WBC/vue-offline/vite-pwa'

    with open(output_dir + '/mapURLs.txt', 'a') as out:

        for tile in mercantile.tiles(*bounds, zooms=zooms):
            url = tileurl + '/{z}/{x}/{y}.png'.format(z = tile.z, x = tile.x, y = tile.y)
            out.write('"' + url + '",\n')
