# strassenlotse maintenance script

In here you will find scripts that handle tasks that need to be done occasionaly or under special circumstances.

Take a close look at it and understand what is going on!

## functionalities

### set codepoints to tags

Will fill out the codepoint section of the tags table. Usefull if you have a large number of new / modified tags.
Can also be done manually inside directus.

### translate project fields with deepL (experiemental)

Do not use out of the box, it needs a lot of owkr to be done! This is a prototyp for filling out translations automatically via deepL.
Needs a deepl Auth Key to work. Can be used with the free version but is limited by the free version quota.

If you ever want to automate translations, this is a good place to start. But will need to be heavily modified!

### create sitemap

Will get all active projects and create a sitemap file. That file must be moved to the public folder.

## how to run

Specify the following variables to your needs (aka DEV / PROD):

DIRECTUS_URL
HOSTNAME
TENANT_ID

Uncoment the function you want to run and start the script with ```node index.mjs```