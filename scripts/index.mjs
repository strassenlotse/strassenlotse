// file
import fs from 'fs'

// directus
import { createDirectus, authentication, readItems, createItem, updateItem, rest } from '@directus/sdk'

//const DIRECTUS_URL = 'https://test.strassenlotse.cloud'
//const HOSTNAME = 'https://www.strassenlotse.hamburg'
//const TENANT_ID = '2'

const directus = createDirectus(DIRECTUS_URL).with(authentication()).with(rest())

// deepl
import * as deepl from 'deepl-node'
const deepl_authKey = "eb09c889-cb74-a5e1-21f5-88bfd6286949:fx"

// materialdesignicons
import { getMeta } from '@mdi/util'

// sitemap
import { SitemapStream, streamToPromise } from 'sitemap' 
import { Readable } from 'stream' 

//await translate()

await createSitemap()

//await addIconCodePoints()

process.exit()

async function addIconCodePoints() {

    // get write access
    await directus.login('friedel@we-build.city', '8yWGTmgaXQ5y5V46OvTA')
    
    // get tags
    let table = 'tags'
    const tags = await directus.request(readItems(table, {fields: ['*'] ,filter: {}, sort: 'id'}))

    const mdi_meta = getMeta()
    
    console.log(mdi_meta[0])

    tags.forEach(async tag => {
        let meta_entry = mdi_meta.find((element) => 'mdi-' + element.name == tag.icon)
        console.log( tag, meta_entry)
        console.log(tag.id,{'codepoint':meta_entry.codepoint}, await directus.request(updateItem(table, tag.id, {'codepoint': meta_entry.codepoint.toString()})))
    })

}

async function createSitemap() {

    // TODO: get only valid projects
    const projects = await directus.request(readItems('projects', {fields: ['*'] ,filter: {'tenant_id': TENANT_ID, "status": "published"}, sort: 'id'}))

    //console.log(projects)

    // basic content
    const basic_links = [   { url: HOSTNAME + '/',  changefreq: 'weekly', priority: 1  },
                            { url: HOSTNAME + '/kontakt/',  changefreq: 'monthly', priority: 0.5  },
                            { url: HOSTNAME + '/faq',  changefreq: 'monthly', priority: 0.5  },
                            { url: HOSTNAME + '/impressum',  changefreq: 'monthly', priority: 0.5  },
                            { url: HOSTNAME + '/datenschutz',  changefreq: 'monthly', priority: 0.5  },
                            { url: HOSTNAME + '/teilnahmebedingungen',  changefreq: 'monthly', priority: 0.5  }
                        ]
    
    var project_links = []

    // create entries for projects
    projects.forEach(element => {
        project_links.push({url: HOSTNAME + '/projekt/'+ element.slug, changefreq: 'monthly', priority: 0.8})
    })

    console.log('project_links', project_links)

    let links = basic_links.concat(project_links)

    // Create a stream to write to
    const stream = new SitemapStream( { hostname: HOSTNAME } )

    // Return a promise that resolves with your XML string
    const content = await streamToPromise(Readable.from(links).pipe(stream)).then((data) =>
        data.toString()
    )

    fs.writeFileSync ('sitemap.xml', content, {flag: 'w+'} ,err => {
        if (err) console.error(err)
    })
}

async function translate() {

    // init deepL API
    const translator = new deepl.Translator(deepl_authKey)
    // available languages directus:deepL -> excluded de-DE:DE since this is the source language
    const usable_languages = {'bg-BG':'BG', 'en-US':'EN-US', 'es-ES':'ES', 'fr-FR':'FR', 'it-IT':'IT', 'pl-PL':'PL', 'pt-BR':'PT-BR', 'ro-RO':'RO', 'ru-RU':'RU', 'uk-UA':'UK'}

    // TODO: get entries to translate
    // do for each _translations-table where origin-table is not deleted
    let table = 'projects_new_translations'

    const projects_translations = await directus.request(readItems(table, {fields: ['*'] ,filter: {}, sort: 'id'}))

    var test = {}

    for (let project_entry in projects_translations) {
        if (!test[projects_translations[project_entry].projects_new_id]) test[projects_translations[project_entry].projects_new_id] = []
        test[projects_translations[project_entry].projects_new_id].push(projects_translations[project_entry])
    }

    //console.log('test', test)

    for (let key in test) {

        // get original entry
        let original = test[key].find((element) => element.languages_code == 'de-DE')

        let found_languages = []

        // cycle all existing entries
        test[key].forEach((element) => {

            // skip german 
            if (element.languages_code == 'de-DE') return null

            found_languages.push(element.languages_code)
            
            // TODO: only translate if previously translated by this script! (user_updated == friedel@we-build.city)
            // only translate new stuff
            if (element.date_update < original.date_update) {
                console.log('found old translation, fixing it!')
                // TODO: update existing entries
            }
        })

        //compare to usable_languages and create missing
        console.log('found_languages', found_languages)

        let missing_languages = []

        for (let available_language_code in usable_languages) {
            if (!found_languages.includes(available_language_code)) missing_languages.push(available_language_code)
        }

        console.log('missing_languages', missing_languages)

        // translate for all usable languages (see: https://www.deepl.com/docs-api/translate-text)
        for (let language in missing_languages) {
            console.log(' processing language: ',  missing_languages[language])

            // formalites: more, less, prefere_less, prefere_more, default
            var claim = await translator.translateText(original.claim, 'DE', usable_languages[missing_languages[language]], { splitSentences:'off' , formality: 'prefer_less', preserveFormatting: true, tagHandling: 'html' })
            var description = await translator.translateText(original.description, 'DE', usable_languages[missing_languages[language]], { formality: 'prefer_less', preserveFormatting: true, tagHandling: 'html' })
            var opening_text = await translator.translateText(original.opening_text, 'DE', usable_languages[missing_languages[language]], { formality: 'prefer_less', preserveFormatting: true, tagHandling: 'html' })
            var contact_text = await translator.translateText(original.contact_text, 'DE', usable_languages[missing_languages[language]], { formality: 'prefer_less', preserveFormatting: true, tagHandling: 'html' })

            var translation_item = {    'projects_new_id': original.projects_new_id,
                                        'languages_code': missing_languages[language],
                                        'claim': claim.text,
                                        'description': description.text,
                                        'opening_text': opening_text.text,
                                        'contact_text': contact_text.text
                                    }

            // save item
            // console.log(translation_item)
            var result = await directus.request(createItem('projects_new_translations', translation_item))
        }
    }

    // get usage
    const usage = await translator.getUsage()
    if (usage.anyLimitReached()) {
        console.log('Translation limit exceeded.')
    }
    if (usage.character) {
        console.log(`Characters: ${usage.character.count} of ${usage.character.limit}`)
    }
    if (usage.document) {
        console.log(`Documents: ${usage.document.count} of ${usage.document.limit}`)
    }

    process.exit()

}