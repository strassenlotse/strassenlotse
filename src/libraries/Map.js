import View from 'ol/View'
import {Map as OLMap} from 'ol'
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer'
import OSM from 'ol/source/OSM'
import {fromLonLat, transformExtent} from 'ol/proj'
import Feature from 'ol/Feature'
import {Circle as CircleStyle, Fill, Icon, Style, Text} from 'ol/style'
import {Cluster, Vector as VectorSource} from 'ol/source'
import {createEmpty, extend, getHeight, getWidth} from 'ol/extent.js'
import GeoJSON from 'ol/format/GeoJSON.js'

export default class Map {

  constructor(DirectusInstance) {
    this.client = DirectusInstance
    this.features = {}
    this.clickFeature = null
    this.clickResolution = null
    this.map = null
    this.target = null
    this.layers = []
  }

  async initMap(target, config) {

    if (this.map) return null

    this.target = target
    this.map = new OLMap({
      target: this.target,
      layers: [
        new TileLayer({
          source: new OSM({
                attributions: [
                  'Daten von <a href="https://www.openstreetmap.org/">OpenStreetMap</a> - Veröffentlicht unter <a href="https://opendatacommons.org/licenses/odbl/">ODbL</a>',
                ],
                url:'https://tile.openstreetmap.de/{z}/{x}/{y}.png',
              })
        }),
      ],
      
      view: new View({
        zoom: parseInt(config.default_zoom),
        minZoom: parseInt(config.min_zoom),
        maxZoom: parseInt(config.max_zoom),
        center: fromLonLat([config.map_center_location[0], config.map_center_location[1]]),
        constrainResolution: true,
        extent: transformExtent(JSON.parse(config.center_extend), 'EPSG:4326', 'EPSG:3857'),
        enableRotation: false,
      }),
    })

    // turn mouse pointer into pointer icon
    this.map.on('pointermove', function(e) {
      var pixel = this.getEventPixel(e.originalEvent)
      var hit = this.hasFeatureAtPixel(pixel)
      this.getViewport().style.cursor = hit ? 'pointer' : ''
    })
    
    this.map.on('click', (event) => {
      
      let foundFeature = false
      
      this.map.forEachFeatureAtPixel(event.pixel,
                (features, layer) => {
                  const clusterMembers = features.get('features')
                  
                  // single feature
                  if (clusterMembers.length == 1) {
                    foundFeature = true
                    target.dispatchEvent(
                      new CustomEvent("foundFeature", {
                        detail: {project_id: clusterMembers[0].values_.project_id}
                      })
                    )
                  }
                  // cluster
                  else if(clusterMembers.length > 1) {
                    // Calculate the extent of the cluster members.
                    const extent = createEmpty()
                    clusterMembers.forEach((feature) =>
                      extend(extent, feature.getGeometry().getExtent())
                    )
                    const view = this.map.getView()
                    const resolution = this.map.getView().getResolution()
                    if (
                      view.getZoom() === view.getMaxZoom() ||
                      (getWidth(extent) < resolution && getHeight(extent) < resolution)
                    ) {
                      // Show an expanded view of the cluster members.
                      this.clickFeature = features[0]
                      this.clickResolution = resolution
                      //clusterCircles.setStyle(clusterCircleStyle)
                    } else {
                      // Zoom to the extent of the cluster members.
                      view.fit(extent, {duration: 500, padding: [50, 50, 50, 50]})
                    }
                  }
                },
                { layerFilter: (layer) => {
                    return (layer.type === new VectorLayer().type && layer.get('name') == 'resultLayer') ? true : false
                  },
                  hitTolerance: 6 
                })
        
      // noting found on click position, might want to hide popup
      if (!foundFeature) {target.dispatchEvent(new CustomEvent("foundNoFeature"))}
    })
  }

  async addFeature(layer, feature) {
  
   if (this.features[layer] == undefined) this.features[layer]=Array()

   this.features[layer].push(new Feature(feature))

   return null
  }

  removeLayer(layer) {
    
    // clear features
    this.features[layer] = Array()

    // remove layer
    this.map.getLayers().forEach(entry => {
      if (entry.get('name') == layer) {
        this.map.removeLayer(entry)
      }
     })
  }

  async addLayer(layer) {

    const clusterSource = new Cluster({
      distance: 30,
      source: new VectorSource({features: this.features[layer]}),
    })

    this.map.addLayer(new VectorLayer({
      source: clusterSource,
      'name': layer,
      'style': this.getMarkerStyle
    }))
  }
  
  getMarkerStyle(feature) {

    let styles = []
    let primarycolor = getComputedStyle(document.documentElement).getPropertyValue('--v-theme-primary').trim()

    // icon cluster
    if (feature.get('features') && feature.get('features').length > 1) {
      const size = feature.get('features').length

      // basic feature circle
      styles.push(new Style({
        zIndex: 1,
        image: new CircleStyle({
                radius: 20,
                fill: new Fill({color: 'rgb(' + primarycolor + ')'})
              })
      }))
      
      let codepointCoordinates = {'0':{'x':-8,'y': -8}, '1':{'x': 8,'y': -8}, '2':{'x':-8,'y': 4}, '3':{'x':8,'y': 4}}

      // add feature icons if less than 4
      if(size <= 4) {
        
        let count = 0

        feature.get('features').forEach(element => {

          styles.push(
              new Style({ // icon
                geometry: feature.getGeometry(),
                text: new Text({text: String.fromCodePoint(parseInt(element.values_.codepoint)),
                  fill: new Fill({
                    color: 'white'
                  }),
                  offsetX: codepointCoordinates[count]['x'],
                  offsetY: codepointCoordinates[count]['y'],
                  font: '10px Material Design Icons'}),
                zIndex:6
            }),
            )
            count++
        })
      }
      else { // display count if more than 4 icons
        styles.push(new Style({
          zIndex:2,
          text: new Text({
            text: size.toString(),
            fill: new Fill({color: 'white'}),
            font: '14px sans-serif'
          })
        }))
      }

      return styles
    }
    
    // single icon
    if (feature.get('features')) feature = feature.get('features')[0]
    
    return [new Style({ // basic marker
                geometry: feature.getGeometry(),
                zIndex:5,
                image: new Icon({
                  anchor: [0.5, 22],
                  anchorXUnits: 'fraction',
                  anchorYUnits: 'pixels',
                  src: 'icons/MapMarker.svg',
                  color: 'rgb(' + primarycolor + ')',
                  scale: 1.5,
                }),
            }),
            new Style({ // icon
              text: new Text({text: String.fromCodePoint(parseInt(feature.values_.codepoint)),
                fill: new Fill({
                  color: 'white'
                }),
                offsetY: -16,
                font: '14px Material Design Icons'}),
              zIndex:6
          })
        ]
  }

}