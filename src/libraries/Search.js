import {fromLonLat, transformExtent} from 'ol/proj'
import { readItem, readItems } from '@directus/sdk'

export default class Search {

  constructor(DirectusInstance) {
    this.client = DirectusInstance
    this.parameter = []
    this.searchTerm = ''
    this.results = []
    this.resultIds = []
    this.mapPOIs = []
    this.projectCache = []
    this.projectFields = 'id, name, slug, seo_description, city, contact_name, districts.districts_id, email, location, main_tag.id, opening_days, opening_events, phone_number, spoken_languages, street, tags.tags_id, website, zipcode, translations.*'
  }

  setParameter (parameter) {
    this.parameter = parameter
  }

  setSearchTerm (term = '') {
    this.searchTerm = term
  }

  async getProjectById(id) {
    // return cache if found
    if(this.projectCache[id]) return this.projectCache[id]

    // get data from db
    await this.client.request(readItem('projects', id, {fields: [this.projectFields]}))
    .then(data => {
      this.projectCache[id] = data
    })

    return this.projectCache[id]
  }

  async getProjectBySlug(slug) {
    var temp = this.projectCache.find(element => (element && element.slug == slug))

    // return cache if found
    if(temp) return temp

    // get data from db
    await this.client.request(readItems('projects', {fields: [this.projectFields], filter: {'slug': slug}}))
    .then(data => {
      temp = data[0]
      this.projectCache[temp.id] = temp
    })

    return temp
  }

  async performSearch() {

    this.resultIds = []
    this.results = []
    this.mapPOIs = []

    if (this.searchTerm.length > 0) {

      await this.client.request(readItems('projects', {fields: ['id'],search: this.searchTerm, filter: this.parameter , sort: 'name'}))
      .then(data => {
        data.forEach(element => {
          this.resultIds.push(element.id)
        })
      })
    }
    else {

      await this.client.request(readItems('projects', {fields: ['id'], filter: this.parameter , sort: 'name'}))
      .then(data => {
        data.forEach(element => {
          this.resultIds.push(element.id)
        })
      })
    }
  }

  async getResults() {

    // get all non cached ids
    var temp = this.resultIds.filter(el => !this.projectCache[el])

    if (temp.length > 0) {
      await this.client.request(readItems('projects', {fields: [this.projectFields], filter: {'id': {'_in': temp}}}))
      .then(data => {
        data.forEach(element => {
          this.projectCache[element.id] = element 
        })
      })
    }

    this.results = this.projectCache.filter(element => this.resultIds.includes(element.id))
    
    return this.results
  }

  // get results optimized for map points
  getResultPOIs() {

    for(const project of this.results) {
      if(project.location) {
        this.mapPOIs.push({
          project_id: project.id,
          name: project.name,
          main_tag: project.main_tag,
          location: fromLonLat([project.location.coordinates[0], project.location.coordinates[1]])
        })
      }
    }

    return this.mapPOIs
  }
}
