import { readItems } from '@directus/sdk'

export default class Tags {

  constructor(DirectusInstance) {
    this.client = DirectusInstance
    this.tagCache = []

    // TODO, should only load one and wait for all tag requests until completely loaded
    this.loadTags()
  }

  async loadTags() {
    
    await this.client.request(readItems('tags', {fields: ['id, name, icon, translations.*, codepoint, searchable'], filter: {'tenant_id': import.meta.env.VITE_PROJECT_TENANT_ID }}))
    .then(data => {
      data.forEach(element => {
        this.tagCache[element.id] = element
      })
    })

    return null
  }

  async getTagById(tagId) {
    if(Object.keys(this.tagCache).length == 0) await this.loadTags()

    return this.tagCache[tagId]
  }

  async getAllSearchableTags() {
    if(Object.keys(this.tagCache).length == 0) await this.loadTags()

    return this.tagCache.filter(element => element.searchable == true)
  }
}
