import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { readItem } from '@directus/sdk'
import piniaPluginPersistedState from "pinia-plugin-persistedstate"
import VueMatomo from 'vue-matomo'
import App from './App.vue'


import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'


// redirect to www
const hostname = window.location.hostname;
if (!hostname.startsWith('localhost') && !hostname.startsWith('www.') && !hostname.startsWith('vorschau.') && !hostname.startsWith('dev.') && !hostname.startsWith('strassenlotse-stuttgart-dev') && !hostname.startsWith('strassenlotse-ng')) {
  const wwwUrl = `https://www.${hostname}${window.location.pathname}${window.location.search}`;
  window.location.replace(wwwUrl);
}

// meta tag handling
import { createHead } from 'unhead'
createHead().push()

// Plugins
import { registerPlugins } from '@/plugins'

const pinia = createPinia()
pinia.use(piniaPluginPersistedState)

const app = createApp(App)
registerPlugins(app)

// directus
import { createDirectus, rest } from '@directus/sdk'

const directus = createDirectus(import.meta.env.VITE_DIRECTUS_URL).with(rest())

app.provide('directus', directus)

import Tags from './libraries/Tags'
app.provide('tags', new Tags(directus))

import Search from './libraries/Search'
app.provide('search', new Search(directus))

app.config.globalProperties.$DIRECTUS_ASSETS_URL = import.meta.env.VITE_DIRECTUS_ASSETS_URL

import { useCommonStore } from '@/stores/common'

directus.request(readItem('tenants', import.meta.env.VITE_PROJECT_TENANT_ID, {fields: ['id, contact_phone_number, contact_mail, translations.*, logo.id, seo_title, seo_description, map_default_zoom, map_min_zoom, map_max_zoom, map_center_location, map_extend, user_default_location, theme_primary_color, theme_secondary_color']}))
        .then(result => {
            
            app.use(createVuetify({
                components,
                directives,
                theme: {
                    themes: {
                        light: {
                            dark: false,
                            colors: {
                                primary: result.theme_primary_color,
                                secondary: result.theme_secondary_color,
                            }
                        },
                    },
                },
            }))

            app.provide('tenant', result)
            app.use(pinia)
            app.provide('commonStore', useCommonStore())

            if ( import.meta.env.VITE_PROJECT_MATOMO_SITE_ID !== 'false' ) {
                app.use(VueMatomo, {
                    host: import.meta.env.VITE_PROJECT_MATOMO_URL,
                    siteId: parseInt(import.meta.env.VITE_PROJECT_MATOMO_SITE_ID),
                    disableCookies: true,
                    preInitActions:[['setDoNotTrack', true]]
                })
            }

            let commonStore = useCommonStore()
            commonStore.setUserDefaultLocation({"lat": result.user_default_location.coordinates[1], "lon": result.user_default_location.coordinates[0]})

            app.mount('#app')
})
