import { createDirectus, rest } from '@directus/sdk';
import { readItems } from '@directus/sdk';
import OSM from 'ol/source/OSM'
import {transformExtent} from 'ol/proj'

const CACHE_VERSION = 10;
const CURRENT_CACHES = {
  all: `cache-all-v${CACHE_VERSION}`
}

self.oninstall = async (e) => {
/*
  let MapURLs = []

  // get all tile urls
  const osmsource = new OSM();
  const mapExtent = transformExtent([9.813, 53.460, 10.182, 53.628], 'EPSG:4326', 'EPSG:3857')
  osmsource.getTileGrid().forEachTileCoord(mapExtent, parseInt(14), function (tileCoord) {
    MapURLs.push('https://tile.openstreetmap.de/' + tileCoord[0] + '/' + tileCoord[1] + '/' + tileCoord[2] + '.png')
  })
//console.log('MapURLs', MapURLs)
  // load all tiles on install
  e.waitUntil(
    caches.open(CURRENT_CACHES.all).then((cache) => {
      return cache.addAll(MapURLs)
    })
  )
*/
}

self.onactivate = (e) => {

  // we have to call this, otherwise the service worker will only work after reloading
  e.waitUntil(self.clients.claim());

  // Delete all caches that aren't named in CURRENT_CACHES.
  // While there is only one cache in this example, the same logic
  // will handle the case where there are multiple versioned caches.
  const expectedCacheNamesSet = new Set(Object.values(CURRENT_CACHES));
  e.waitUntil(
    caches.keys().then((cacheNames) =>
      Promise.all(
        cacheNames.map((cacheName) => {
          if (!expectedCacheNamesSet.has(cacheName)) {
            // If this cache name isn't present in the set of
            // "expected" cache names, then delete it.
            console.log("Deleting out of date cache:", cacheName);
            return caches.delete(cacheName);
          }
        }),
      ),
    ),
  );

}

self.onfetch = (e) => {
  //console.log('onfetch: ' +  e.request.method + ' -> ' +  e.request.url)
  if (e.request.method != 'GET') return null

  // filter weird stuff like chrome extensions
  if(!e.request.url.startsWith('http')) {
    e.respondWith(fetch(e.request))
    return null
  }

  // will create fake tiles for debugging
  const url = new URL(e.request.url)
  if (url.host === 'fake-server') {
      e.respondWith(generateImage(url))
      return null
  }
  
  e.respondWith(NetworkThenCache(e.request))
}

self.onpush = (e) => {
  console.log('onpush: ', e)
}

self.onsync = (e) => {
  console.log('onsync: ', e)
}

self.onperiodicsync = (e) => {
  if (e.tag === "get-latest-news") {
    console.log('Periodic Sync called for get-latest-news')
    // TODO: fetch latest updates
    //e.waitUntil(fetchAndCacheLatestNews());
  }
  console.log('onperiodicsync: ', e.tag)
}


self.onmessage = async (e) => {
  console.log('onmessage: ', e)

  if (e.data == 'getCachesSize') {
    
    // returns approximate size of all caches (in kilobytes)
    let size =  await 
    caches.keys().then(a => {
      return Promise.all(
        a.map(n => caches.open(n).then(c => cacheSize(c)))
      ).then(a => a.reduce((acc, n) => acc + n, 0))
    })
    self.postMessage({msg: 'getCachesSize', value: (size / 1024 / 1024).toFixed(2) +  ' MB' })

    return 
  }
  else if (e.data === 'getProjects') {

    const directus = createDirectus(import.meta.env.VITE_DIRECTUS_URL).with(rest())
    const result = await directus.request(readItems('projects_new', {fields: ['*.*']}))
    self.postMessage({msg: 'getProjects', value: result})
  }
  else if (e.data === 'getTileURLs') {
    self.postMessage({msg: 'getTileURLs', value: TILE_URL})
  }
}

async function NetworkThenCache(request) {

  let cache = await caches.open(CURRENT_CACHES.all)
  let networkError = null
  let networkResponse = await fetch(request.clone()).catch((error) => {
    console.error("Error in fetch handler:", error)
    networkError = error
    return false
  }) 

 // console.log("Response for %s from network is: %O", request.url, networkResponse)

  // bad result, try cache
  if (networkResponse === false || networkResponse.status >= 400) {
    return await cache.match(request)
      .then((cacheResponse) => {
        if (cacheResponse) {
          console.log("Found response in cache:", cacheResponse)
          return cacheResponse
        }
        
        return networkError
      })
  }

  // good result, put in cache for next time
  //console.log("Caching the response to", request.url)
  cache.put(request, networkResponse.clone())
  
  return networkResponse

}

async function CacheThenNetwork(request) {

  return caches.open(CURRENT_CACHES.all).then((cache) => {
    return  cache
            .match(request)
            .then((response) => {
              if (response) {
                console.log("Found response in cache:", response)
                return response
              }

              //console.log("No response for %s found in cache. About to fetch from network:", request.url)

              // We call .clone() on the request since we might use it in a call to cache.put() later on.
              // Both fetch() and cache.put() "consume" the request, so we need to make a copy.
              return fetch(request.clone()).then((response) => {
                //console.log("Response for %s from network is: %O", request.url, response)

                if (response.status < 400) {
                  // This avoids caching responses that we know are errors
                  // (i.e. HTTP status code of 4xx or 5xx).
                
                  //console.log("Caching the response to", request.url)
                  // We call .clone() on the response to save a copy of it
                  // to the cache. By doing so, we get to keep the original
                  // response object which we will return back to the controlled
                  // page.
                  // https://developer.mozilla.org/en-US/docs/Web/API/Request/clone
                  cache.put(request, response.clone());
                } else console.log("Not caching the response to", request.url)
                
                // Return the original response object, which will be used to fulfill the resource request.
                //console.log('return fetched resource', response)
                return response;
              })
            })
            .catch((error) => {
              //console.error("Error in fetch handler:", error)
              throw error
            }) 
  })
}

function cacheSize(c) {
  return c.keys().then(a => {
    return Promise.all(
      a.map(req => c.match(req).then(res => res.clone().blob().then(b => b.size)))
    ).then(a => a.reduce((acc, n) => acc + n, 0));
  });
}

async function generateImage(url) {
  const [, x, y, z] = url.pathname
      .split('/')
      .map(i => Number.parseInt(i))
  const canvas = new OffscreenCanvas(256, 256);
  var context = canvas.getContext("2d");
  context.fillStyle = 'orange';
  context.fillRect(8, 8, 240, 240);
  context.font = "20px Arial";
  context.fillStyle = 'black';
  context.fillText(`${x}, ${y}, ${z}`, 16, 128);
  const blob = await canvas.convertToBlob()
  return new Response(blob)
}