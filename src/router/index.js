import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      alias: '/home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/karte/:tag_id?',
      name: 'map',
      alias: '/map/:tag_id?',
      component: () => import('../views/MapView.vue')
    },
    {
      path: '/projekt/:projectSlug',
      name: 'project',
      alias: '/project/:projectSlug',
      component: () => import('../views/ProjectView.vue')
    },
    {
      path: '/dokumente',
      name: 'documents',
      alias: '/documents',
      component: () => import('../views/DocumentsView.vue')
    },
    {
      path: '/impressum',
      name: 'imprint',
      alias: '/imprint',
      component: () => import('../views/ImprintView.vue')
    },
    {
      path: '/datenschutz',
      name: 'privacy',
      alias: '/privacy',
      component: () => import('../views/PrivacyView.vue')
    },
    {
      path: '/faq',
      name: 'faq',
      alias: '/faq',
      component: () => import('../views/FAQView.vue')
    },
    {
      path: '/favoriten',
      name: 'favorites',
      alias: '/favorites',
      component: () => import('../views/FavoritesView.vue')
    },
    {
      path: '/ueber_uns',
      name: 'about_us',
      alias: '/about_us',
      component: () => import('../views/AboutUsView.vue')
    },
    {
      path: '/teilnahmebedingungen',
      name: 'conditions_of_participation',
      alias: '/conditions_of_participation',
      component: () => import('../views/ConditionsOfParticipation.vue')
    },
    {
      path: '/kontakt',
      name: 'contact',
      alias: '/contact',
      component: () => import('../views/ContactView.vue')
    },
    {
      path: '/registrierung',
      name: 'registration',
      alias: '/registration',
      component: () => import('../views/RegistrationView.vue')
    },
    {
      path: '/zustaendigkeit',
      name: 'authority',
      alias: '/authority',
      component: () => import('../views/AuthorityView.vue')
    }
  ]
})

export default router
