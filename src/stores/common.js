import { defineStore } from 'pinia'

export const useCommonStore = defineStore('common', {
  state: () => ({
    language: 'de-DE',
    userDefaultLocation: null,
    userLocation: null
  }),
  persist: true,
  actions: {
    setLanguage(language) {
      this.language = language
    },
    getLanguage() {
      return this.language
    },
    setUserDefaultLocation(userDefaultLocation){
      this.userDefaultLocation = userDefaultLocation
    },
    setUserLocation(userLocation) {
      this.userLocation = userLocation
    },
    getUserLocation() {
      if (!this.userLocation) return this.userDefaultLocation
      return this.userLocation
    },
    hasRealUserLocation() {
      return this.userLocation ? true: false
    }
  }
})