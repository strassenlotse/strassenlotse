import { defineStore } from 'pinia'

export const useFavoritesStore = defineStore('favorites', {
  state: () => ({
    favorites: [],
  }),
  persist: true,
  actions: {
    has(id) {
      return this.favorites[id] ? true : false
    },
    add(project) {
      this.favorites[project.id] = project
    },
    remove(project) {
      this.favorites[project.id] = null
    },
    getById(id) {
      return this.favorites[id]
    },
    getAll() {
      return this.favorites
    },
    toggle(project) {
      if (this.favorites[project.id]) this.favorites[project.id] = null
      else this.favorites[project.id] = project
    }
  }
})