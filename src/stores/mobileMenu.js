import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useMobileMenuStore = defineStore('mobileMenu', () => {
  const showList = ref(false)
  
  function toggle() {
    showList.value = !showList.value
  }

  return { showList, toggle }
})