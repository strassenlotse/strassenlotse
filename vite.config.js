import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'
import VueDevTools from 'vite-plugin-vue-devtools'
import vuetify from 'vite-plugin-vuetify'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vuetify(),
    VueDevTools(),
    VitePWA({
      devOptions: {
        enabled: true,
        type: 'module'
      },
      srcDir: 'src',
      filename: 'my-worker.js',
      injectRegister: 'auto',
      strategies:'injectManifest', // we choose this because our serviceWorker will do more than just cache static files!
      registerType: 'prompt', // we choose this because this is more user friendly once we have forms
      workbox: {
        globPatterns: ['**/*.{js,css,html,ico,png,svg}']
      },
      injectManifest: { // otherwise we get an "Error: Unable to find a place to inject the manifest" error when building
        injectionPoint: undefined
      },
      manifest: {
        name: 'Strassenlotse',
        short_name: 'Strassenlotse',
        description: 'Strassenlotse',
        theme_color: '#ffffff',
        icons: [  {
                    "src": "/android-chrome-192x192.png",
                    "sizes": "192x192",
                    "type": "image/png"
                  },
                  {
                    "src": "/android-chrome-512x512.png",
                    "sizes": "512x512",
                    "type": "image/png"
                  }]
      }

    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
